//
//  prefix_header.h
//  scanView
//
//  Created by Macmini on 2019/8/6.
//  Copyright © 2019 wenzbph. All rights reserved.
//

#ifndef prefix_header_h
#define prefix_header_h

#define AdjustsScrollViewInsetNever(controller,view) if(@available(iOS 11.0, *)) {view.contentInsetAdjustmentBehavior = UIApplicationBackgroundFetchIntervalNever;} else if([controller isKindOfClass:[UIViewController class]]) {controller.automaticallyAdjustsScrollViewInsets = false;}
#define Screen_Height [UIScreen mainScreen].bounds.size.height
#define Screen_Width [UIScreen mainScreen].bounds.size.width
#define Screen_NavBarHeight  self.navigationController.navigationBar.frame.size.height
#define Screen_StatusBarHeight [[UIApplication sharedApplication] statusBarFrame].size.height

#endif /* prefix_header_h */
