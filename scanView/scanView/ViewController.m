//
//  ViewController.m
//  scanView
//
//  Created by Macmini on 2018/12/13.
//  Copyright © 2018年 wenzbph. All rights reserved.
//

#import "ViewController.h"
#import "goodsModel.h"
#import "NSObject+model.h"
#import <objc/message.h>
#import "Person.h"
#import "NSDictionary+CreateProperty.h"
#import "NSObject+GetProperty.h"
#import "scanView-Swift.h"
#import "ProViewController.h"
#import <Photos/Photos.h>
//#import "ProViewController.h"
#import "Person+UsePrivateMethod.h"
#import "CustomeView.h"
#import "ChartView.h"

#import "UITableView+extendDelegate.h"
#import "UIScrollView+gradient.h"
#import "UIImage+Common.h"

#import "AFNetworking.h"
#import "ViewController+ScrollviewExtendDelegate.h"



@interface ViewController ()<UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate>
{
    dispatch_semaphore_t semaphore;
}
@property (nonatomic , strong)Person *people;
@property (nonatomic , strong)UITextField *nameText;
@property (nonatomic , strong)NSMutableArray *dataArr;

@property (nonatomic , strong) PHFetchResult<PHAsset *> *assets;


@property (nonatomic , strong)  CustomeView *redView;

@property (nonatomic , strong)  ChartView *chartView;

@property (assign,nonatomic) BOOL isdowmdrap;

@property (assign,nonatomic) BOOL islimitLev;
@property (assign,nonatomic) NSInteger ticketSurplusCount;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _isdowmdrap = false;
    // Do any additional setup after loading the view, typically from a nib.
    NSArray *dic = @[@{
                          @"order_sn":@"2018121902085931",
                          @"age":@1,
                          @"goodsItem":@[@{
                                             @"name":@"语文",
                                             @"sub_name":@"111",
                                             
                                            },
                                          @{
                                             @"name":@"数学",
                                             @"sub_name":@"111"
                                            },
                                           @{
                                             @"name":@"美术",
                                             @"sub_name":@"333"
                                             },
                                            @{
                                             @"name":@"体育",
                                             @"sub_name":@"222"
                                             }],
                          @"isA":@false
                          },
                        @{
                            @"order_sn":@"2018121902085931",
                            @"age":@1,
                            @"goodsItem":@[
                                    @{
                                        @"name":@"语文",
                                        @"sub_name":@"111",
                                        
                                        @"goodsInfo": @{
                                                @"name":@"语文",
                                                @"sub_name":@"111"
                                                }
                                        },
                                    @{
                                        @"name":@"英语",
                                        @"sub_name":@"111",
                                      
                                        @"goodsInfo": @{
                                                @"name":@"语文",
                                                @"sub_name":@"111"
                                                }
                                        }]
                        },
                          ];
   self.dataArr = [NSMutableArray new];
//    NSDictionary *defaultResult = dic[0];
    for (NSDictionary *result in dic) {
    
//        goodsModel *model = [[goodsModel alloc] init];
//        [model setValuesForKeysWithDictionary:result];

       goodsModel *model = [goodsModel initModelWithDict:result];
       [self.dataArr addObject:model];

    }
    
    goodsModel *model = self.dataArr.firstObject;
    NSLog(@"%@--%@",model.age,self.dataArr[5]);
    
    
//    [defaultResult createPrope];
    
    
//    goodsModel *item = arr[0];
//    
//    NSLog(@"arr%@mode%lu",arr[0],(unsigned long)item.goodsItem.count);
 
    
    Person *p = [[Person alloc] init];
    p.name = @"jack";
    _people = p;

    p.getCardInfo = ^(NSDictionary *cardInfo) {
        
    };
    
//    [p addObserver:self forKeyPath:@"name" options:NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld context:nil];
//    [p performSelector:@selector(eat)];
    
    [p eat:@"3"];
    
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0]] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController setNavigationBarHidden:true animated:true];
//    id pro = [NSClassFromString(@"ProViewController") new];
//    [pro jl_getProperties];
    
//    PHFetchResult<PHAssetCollection *> *videos = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum subtype:PHAssetCollectionSubtypeSmartAlbumVideos options:nil];//这应该是获取相片中的一个集合  获取所有相片中所有视频的集合
//    /**
//     这一步应该是获取每一个视频集合里的所有本地视频的文件
//
//     */
//    NSMutableArray *coll = [NSMutableArray new];
//    for (PHAssetCollection *collection in videos) {
//
//        NSLog(@"videosname---%@",collection.localizedTitle);
//        self.assets = [PHAsset fetchAssetsInAssetCollection:collection options:nil];
//
//        [coll addObject:self.assets];
//    }
//
//
//    PHVideoRequestOptions *options = [[PHVideoRequestOptions alloc] init];
//    // 同步获得图片, 只会返回1张图片
//
//
//    for (int i = 0;i<coll.count;i++) {
//        PHFetchResult<PHAsset *> *Videoassets = coll[i];
//        if (Videoassets) {
//            for (PHAsset *asset in Videoassets) {
//                [[PHCachingImageManager defaultManager] requestAVAssetForVideo:asset options:options resultHandler:^(AVAsset * _Nullable asset, AVAudioMix * _Nullable audioMix, NSDictionary * _Nullable info) {
//                    AVURLAsset *urlAsset = (AVURLAsset *)asset;
//                    NSURL *url = urlAsset.URL;
//                    NSLog(@"%@ %@ %@ %@",asset,info,audioMix,url);
//
//                }];
//            }
//        }
//
//
//    };
    
    
  
    
    
   
    
//    PHFetchResult<PHAssetCollection *> *cameraRolls = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum subtype:PHAssetCollectionSubtypeSmartAlbumUserLibrary options:nil];
//
//    NSLog(@"cameraRolls--%lu",(unsigned long)cameraRolls.count);
    
//    self.redView = [[UIView alloc] initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, 140)];
    
//    [self.view addSubview:self.chartView];


//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refresh) name:@"refreshListView" object:nil];
    
//    [p addObserver:<#(nonnull NSObject *)#> forKeyPath:<#(nonnull NSString *)#> options:<#(NSKeyValueObservingOptions)#> context:<#(nullable void *)#>]
    
    
    
//    self.scrollTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStylePlain];
//    self.scrollTable.delegate = self;
//    self.scrollTable.dataSource = self;
//
//    [self.view addSubview:self.scrollTable];
//    [self.scrollTable registerClass:[UITableViewCell class] forCellReuseIdentifier:@"CellTableIdentifier"];

    

    AdjustsScrollViewInsetNever(self, self.scrollTable);
    self.scrollTable.delegate = self;

    [self.view addSubview:self.customeView];

    [self.view addSubview:self.topView];
    
    _space =CGRectGetMinY(self.topView.text.frame) -   CGRectGetMinY(self.customeView.text.frame);
    _bwidth = CGRectGetMaxX(self.topView.text.frame) - CGRectGetMaxX(self.customeView.text.frame);
    
    _linjiePoint = CGRectGetMinY(self.topView.text.frame) - (CGRectGetMaxY(self.customeView.frame)/2);
    
    _topTextMinY = CGRectGetMinY(self.topView.text.frame);
    _navTextMinY = CGRectGetMinY(self.customeView.text.frame);
    
    
   
    
    //3、修改局部变量，要用__block
//    __block NSInteger testNum3 = 10;
//    void (^block3)(void) = ^() {
//        NSLog(@"读取局部变量：%ldd",(long) testNum3); //打印：20
//        testNum3 = 1000;
//        NSLog(@"修改局部变量：%ldd",(long) testNum3); //打印：1000
//    };
//    testNum3 = 20;
//    block3();
//    testNum3 = 30;
//    NSLog(@"testNum最后的值：%ldd",(long) testNum3);//打印：30
//    [self semaphoreSync];
    [self initTicketStatusSave];
//    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(100, 100, 50, 50)];
//    btn.backgroundColor = [UIColor redColor];
//    [btn addTarget:self action:@selector(buy) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:btn];
}



/**
 * semaphore 线程同步
 */
- (void)semaphoreSync {
    
    NSLog(@"currentThread---%@",[NSThread currentThread]);  // 打印当前线程
    NSLog(@"semaphore---begin");
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    
    __block int number = 0;
    dispatch_async(queue, ^{
        // 追加任务 1
        [NSThread sleepForTimeInterval:2];              // 模拟耗时操作
        NSLog(@"1---%@",[NSThread currentThread]);      // 打印当前线程
        
        number = 100;
        
        dispatch_semaphore_signal(semaphore);
    });
    
    long wait = dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    
    NSLog(@"semaphore---end,number = %d",number);
}

/**
 * 线程安全：使用 semaphore 加锁
 * 初始化火车票数量、卖票窗口（线程安全）、并开始卖票
 */
- (void)initTicketStatusSave {
    NSLog(@"currentThread---%@",[NSThread currentThread]);  // 打印当前线程
    NSLog(@"semaphore---begin");
    
    semaphore = dispatch_semaphore_create(1);
    
    self.ticketSurplusCount = 50;
    
    // queue1 代表北京火车票售卖窗口
    dispatch_queue_t queue1 = dispatch_queue_create("net.bujige.testQueue1", DISPATCH_QUEUE_SERIAL);
    // queue2 代表上海火车票售卖窗口
    dispatch_queue_t queue2 = dispatch_queue_create("net.bujige.testQueue2", DISPATCH_QUEUE_SERIAL);
    
    __weak typeof(self) weakSelf = self;
    dispatch_async(queue1, ^{
        [weakSelf saleTicketSafe];
    });
    
    dispatch_async(queue2, ^{
        [weakSelf saleTicketSafe];
    });
}

/**
 * 售卖火车票（线程安全）
 */
- (void)saleTicketSafe {
    while (1) {
        // 相当于加锁
        long wait = dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
        NSLog(@"当前%ld",wait);
        if (self.ticketSurplusCount > 0) {  // 如果还有票，继续售卖
            self.ticketSurplusCount--;
            NSLog(@"%@", [NSString stringWithFormat:@"剩余票数：%ld 窗口：%@", (long)self.ticketSurplusCount, [NSThread currentThread]]);
            [NSThread sleepForTimeInterval:0.2];
        } else { // 如果已卖完，关闭售票窗口
            NSLog(@"所有火车票均已售完");
            
            // 相当于解锁
            dispatch_semaphore_signal(semaphore);
            break;
        }
        
        // 相当于解锁
        dispatch_semaphore_signal(semaphore);
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellTableIndentifier = @"CellTableIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellTableIndentifier];
//      UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellTableIndentifier forIndexPath:indexPath];
   
    //初始化单元格
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellTableIndentifier];
        //自带有两种基础的tableView样式，UITableViewCellStyleValue1、2. 后面的文章会讲解自定义样式
    }
//     NSLog(@"%@",cell);
//    cell.backgroundColor = [UIColor purpleColor];

    // cell的数据填充(cell的category方法)
    cell.detailTextLabel.text = @"1111";
    cell.textLabel.text = @"测试";
    
    return cell;
}



- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 15;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:true];
}

- (void)refresh{
    
    
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context{
    NSLog(@"%@----%@",keyPath,change);
}

+ (BOOL)resolveInstanceMethod:(SEL)sel {
//    if (sel == @selector(buy:)) {//如果是执行foo函数，就动态解析，指定新的IMP
//        class_addMethod([self class], sel, (IMP)fooMethod, "v@:");
//        return YES;
//    }
//    return [super resolveInstanceMethod:sel];
    
    return true;
}

//void fooMethod(id obj, SEL _cmd) {
//    NSLog(@"Doing foo");//新的foo函数
//}

//重定向类方法：返回一个类对象
+ (id)forwardingTargetForSelector:(SEL)aSelector{
    if (aSelector == @selector(buy:)) {

        return [Person class];
    }
    return [super forwardingTargetForSelector:aSelector];
}


//重定向实例方法：返回类的实例
- (id)forwardingTargetForSelector:(SEL)aSelector{
    if (aSelector == @selector(eat:)) {
        return self.people;
    }else if (aSelector == @selector(buy)){
        return self.people;
    }
    return [super forwardingTargetForSelector:aSelector];
}


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
//    [_people willChangeValueForKey:@"name"];
//    _people.name = @"lucy";
//    [_people didChangeValueForKey:@"name"];

//    [self.navigationController pushViewController:[TextViewController new] animated:true];
//    NSDictionary *dic = @{@"name":@"jack",@"age":@112,@"friends":@[]};
//
//    ProViewController *vc = [[ProViewController alloc] init];
//    vc.data = dic;
//    [self.navigationController pushViewController:vc animated:true];
    
//    UITouch *tuch = touches.anyObject;
//    CGPoint point = [tuch locationInView:self.view];
    
   
    
//    CABasicAnimation *anim = [CABasicAnimation animation];
//
//    anim.keyPath = @"position.y";
//    anim.fromValue = @64;
//    anim.toValue = @300;
//    anim.fromValue = [NSValue valueWithCGPoint:CGPointMake(0, 0)];;
//    anim.toValue = [NSValue valueWithCGPoint:CGPointMake(200, 464)];
//    anim.duration = 2.0;
//    anim.removedOnCompletion = NO;
//    anim.fillMode = kCAFillModeForwards;
//    [self.redView.layer addAnimation:anim forKey:@"nil"];
    
    /*
     animateWithDuration 动画持续时间
     delay 动画延迟执行的时间
     usingSpringWithDamping 震动效果，范围0~1，数值越小震动效果越明显
     initialSpringVelocity 初始速度，数值越大初始速度越快
     options 动画的过渡效果
     */
    
//    [UIView animateWithDuration:2//动画持续时间
//
//                     animations:^{
//                         //执行的动画
//                          self.redView.frame = CGRectMake(0,64, self.view.frame.size.width, 140);
//                     }
//                     completion:^(BOOL finished) {
//                         //动画执行提交后的操作
//        }];
 
    
    //像翻书效果
//    [UIView beginAnimations:@"test" context:nil];
//
//    [UIView setAnimationDuration:1.0];
//    [UIView setAnimationRepeatCount:1];
//    [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.redView cache:YES];
//
//    [UIView commitAnimations];
    
   
//    [self.redView show];

    
    self.people = [[Person alloc] init];
    
//    SEL selector = NSSelectorFromString(@"eat:");
    
//    ((void(*)(id,SEL,NSInteger)) objc_msgSend)(self,selector,2);
    

    [self performSelector:@selector(eat:) withObject:@"2"];
    
}




- (void)threadNotSafe {
   __block NSInteger total = 0;
    for (NSInteger index = 0; index < 3; index++) {
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            total += 1;
            NSLog(@"total: %ld", (long)total);
            total -= 1;
            NSLog(@"total: %ld", (long)total);
        });
    }
}


- (CustomeView *)redView{
    if (!_redView) {
        self.redView = [[CustomeView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        self.redView.backgroundColor = [UIColor greenColor];
        _redView.contrc = self;
        [self.view addSubview:self.redView];
    }
    return _redView;
}

- (ChartView *)chartView{
    
    if (!_chartView) {
        _chartView = [[ChartView alloc] initWithFrame:CGRectMake(0, 100, self.view.frame.size.width, self.view.frame.size.width)];
    }
    return _chartView;
    
}

- (jianbianView *)topView{
    
    if (!_topView) {
        _topView = [[jianbianView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, 110*(Screen_Width/375))];
    }
    
    return _topView;
}

- (CustomeNavView *)customeView{
    if (!_customeView) {
        
        _customeView = [[CustomeNavView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_NavBarHeight+Screen_StatusBarHeight)];
        _customeView.alpha = 0;
//        _customeView.text.alpha = 0;
    }
    return _customeView;
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
