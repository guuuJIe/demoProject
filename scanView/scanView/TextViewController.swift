//
//  TextViewController.swift
//  scanView
//
//  Created by Macmini on 2019/1/21.
//  Copyright © 2019 wenzbph. All rights reserved.
//

import UIKit

class TextViewController: UIViewController,UITextFieldDelegate {
   
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.red
        // Do any additional setup after loading the view.
        var _ : UITextField = {
            let pwdText = UITextField.init(frame: CGRect.init(x: 0, y: 100, width: 150, height: 40))
            pwdText.placeholder = "请输入密码"
            view.addSubview(pwdText)
            pwdText.font = UIFont.systemFont(ofSize: 17)
            pwdText.keyboardType = UIKeyboardType.default
            pwdText.delegate = self;
            return pwdText
        }()
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        textField.resignFirstResponder();
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
