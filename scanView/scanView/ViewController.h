//
//  ViewController.h
//  scanView
//
//  Created by Macmini on 2018/12/13.
//  Copyright © 2018年 wenzbph. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "jianbianView.h"
#import "CustomeNavView.h"


#define topViewHeight CGRectGetMinY(self.topView.text.frame)
#define navViewHeight CGRectGetMinY(self.customeView.text.frame)
@interface ViewController : UIViewController


@property (nonatomic) jianbianView *topView;

@property (nonatomic) CustomeNavView  *customeView;

@property (nonatomic) UITableView *scrollTable;

@property (nonatomic,assign) CGFloat space;//上下的距离

@property (nonatomic,assign) CGFloat bwidth;//多于宽度的k距离

@property (nonatomic,assign) CGFloat linjiePoint;

@property (nonatomic,assign) CGFloat topTextMinY;

@property (nonatomic,assign) CGFloat navTextMinY;
@property (nonatomic,assign) CGFloat offSetY;
@end

