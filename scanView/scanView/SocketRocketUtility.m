//
//  SocketRocketUtility.m
//  scanView
//
//  Created by Macmini on 2019/1/26.
//  Copyright © 2019 wenzbph. All rights reserved.
//

#define WeakSelf(ws) __weak __typeof(&*self)weakSelf = self

#import "SocketRocketUtility.h"
#import <SocketRocket.h>

static NSString * kNeedPayOrderNote = @"kNeedPayOrderNote";//发送的通知名称

static NSString * kWebSocketDidOpenNote = @"kWebSocketDidOpenNote";
@interface SocketRocketUtility()<SRWebSocketDelegate>

@property (nonatomic , assign)int index;
@property (nonatomic , strong)SRWebSocket *socket;
@property (nonatomic , assign)NSTimeInterval reConnectTime;
@property (nonatomic , strong)NSTimer *heartBeat;
@end

@implementation SocketRocketUtility

+ (SocketRocketUtility *)instace{
    static SocketRocketUtility *instance = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        instance = [[SocketRocketUtility alloc] init];
    });
    
    return instance;
}

- (void)SRWebSocketOpenWithURLString:(NSString *)urlString{
    if (self.socket) {
        return;
    }
    
    if (!urlString) {
        return;
    }
    
    //SRWebSocketUrlString 就是websocket的地址 写入自己后台的地址
    self.socket = [[SRWebSocket alloc] initWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]]];
    self.socket.delegate = self;
    
    [self.socket open];//开始连接
}

#pragma mark --Method---
- (void)SRWebSocketClose{
    if (self.socket) {
        [self.socket close];
        self.socket = nil;
        
        [self destoryHeartBeat];
    }
}

//初始化心跳
- (void)initHeartBeat{
    WeakSelf(ws);
    dispatch_async(dispatch_get_main_queue(), ^{
        
        self.heartBeat = [NSTimer scheduledTimerWithTimeInterval:3*60 repeats:true block:^(NSTimer * _Nonnull timer) {
            //和服务端约定好发送什么作为心跳标识，尽可能的减小心跳包大小
            [weakSelf sendData:@"heart"];
        }];
        
        [[NSRunLoop currentRunLoop] addTimer:self.heartBeat forMode:NSRunLoopCommonModes];
    });
}

//取消心跳
- (void)destoryHeartBeat{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.heartBeat) {
            [self.heartBeat invalidate];
            self.heartBeat = nil;
        }
    });
}

//pingPong机制
- (void)ping{
    [self.socket sendPing:nil];
}

- (void)sendData:(id)data{
    WeakSelf(ws);
    dispatch_queue_t queue = dispatch_queue_create("zy", NULL);
    dispatch_async(queue, ^{
        
        if (weakSelf.socket != nil) {
#warning impo1
            //只有当open的时候才能调用send方法，不能会崩
            if (weakSelf.socket.readyState == SR_OPEN) {
                [weakSelf.socket send:data];
            }else if (weakSelf.socket.readyState == SR_CONNECTING){
                
            }else if (weakSelf.socket.readyState == SR_CLOSED || weakSelf.socket.readyState == SR_CLOSING){
                
            }
        }else{
            NSLog(@"没网络，发送失败，一旦断网 socket 会被我设置 nil 的");
        }

    });
}

//重连机制
- (void)reConnect{
    [self SRWebSocketClose];
    
    //超过一分钟就不再重连 所以只会重连5次 2^5 = 64
    if (_reConnectTime > 64) {
        return;
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(_reConnectTime * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.socket = nil;
//        [self SRWebSocketopen]
    });
}

#pragma mark - socket delegate
- (void)webSocketDidOpen:(SRWebSocket *)webSocket {
    NSLog(@"连接成功，可以与服务器交流了,同时需要开启心跳");
    //每次正常连接的时候清零重连时间
    _reConnectTime = 0;
    //开启心跳 心跳是发送pong的消息 我这里根据后台的要求发送data给后台
    [self initHeartBeat];
    [[NSNotificationCenter defaultCenter] postNotificationName:kWebSocketDidOpenNote object:nil];
}

- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error {
    NSLog(@"连接失败，这里可以实现掉线自动重连，要注意以下几点");
    NSLog(@"1.判断当前网络环境，如果断网了就不要连了，等待网络到来，在发起重连");
    NSLog(@"2.判断调用层是否需要连接，例如用户都没在聊天界面，连接上去浪费流量");
    NSLog(@"3.连接次数限制，如果连接失败了，重试10次左右就可以了，不然就死循环了。");
          _socket = nil;
          //连接失败就重连
    [self reConnect];
}

- (void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean {
    NSLog(@"被关闭连接，code:%ld,reason:%@,wasClean:%d",code,reason,wasClean);
              //断开连接 同时销毁心跳
    [self SRWebSocketClose];
}
          
    /*
     该函数是接收服务器发送的pong消息，其中最后一个是接受pong消息的，
     在这里就要提一下心跳包，一般情况下建立长连接都会建立一个心跳包，
     用于每隔一段时间通知一次服务端，客户端还是在线，这个心跳包其实就是一个ping消息，
     我的理解就是建立一个定时器，每隔十秒或者十五秒向服务端发送一个ping消息，这个消息可是是空的
     */
-(void)webSocket:(SRWebSocket *)webSocket didReceivePong:(NSData *)pongPayload{
    NSString *reply = [[NSString alloc] initWithData:pongPayload encoding:NSUTF8StringEncoding];
    NSLog(@"reply===%@",reply);
}
          
- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message  {
    //收到服务器发过来的数据 这里的数据可以和后台约定一个格式 我约定的就是一个字符串 收到以后发送通知到外层 根据类型 实现不同的操作
    NSLog(@"%@",message);
    [[NSNotificationCenter defaultCenter] postNotificationName:kNeedPayOrderNote object:message];
}


- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
