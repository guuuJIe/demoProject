//
//  ChartView.m
//  scanView
//
//  Created by Macmini on 2019/7/23.
//  Copyright © 2019 wenzbph. All rights reserved.
//

#import "ChartView.h"

#define bounceX 20

#define bounceY 20
@implementation ChartView

- (instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];//防止初始化过程中发生改变，返回了不同对象。
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
//        [self createLabelY];
//        [self createLabelX];
        
    }
    
    return self;
    
}

- (void)drawRect:(CGRect)rect{
    
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    CGContextSetLineWidth(context, 2.0);
//    CGContextSetRGBStrokeColor(context, 1, 0, 0, 1);
//    CGContextMoveToPoint(context, bounceX, bounceY);
//    CGContextAddLineToPoint(context, bounceX, rect.size.height - bounceY);
//    CGContextAddLineToPoint(context,rect.size.width -  bounceX, rect.size.height - bounceY);
//    CGContextAddLineToPoint(context, rect.size.width - bounceX,  rect.size.height - bounceY);
    
//    CGContextAddLineToPoint(context, rect.size.width - bounceX,  bounceY);
//    CGContextAddLineToPoint(context, bounceX,  bounceY);
//    CGContextStrokePath(context);
    
    
    
    
    [[UIColor redColor] set];

    UIBezierPath* path = [UIBezierPath bezierPath];

    path.lineWidth     = 5.f;
    path.lineCapStyle  = kCGLineCapRound;
    path.lineJoinStyle = kCGLineCapRound;

    [path moveToPoint:CGPointMake(50, 226)];
    // 给定终点和控制点绘制贝塞尔曲线
    [path addQuadCurveToPoint:CGPointMake(234.375, 618) controlPoint:CGPointMake(Screen_Width/2, 420)];
    
    

    [path stroke];
    
}


#pragma mark 创建x轴的数据
- (void)createLabelX{
    CGFloat  month = 12;
    for (NSInteger i = 0; i < month; i++) {
        UILabel * LabelMonth = [[UILabel alloc]initWithFrame:CGRectMake((self.frame.size.width - 2*bounceX)/month * i + bounceX, self.frame.size.height - bounceY + bounceY*0.3, (self.frame.size.width - 2*bounceX)/month- 5, bounceY/2)];
        //       LabelMonth.backgroundColor = [UIColor greenColor];
        LabelMonth.tag = 1000 + i;
        LabelMonth.text = [NSString stringWithFormat:@"%ld月",i+1];
        LabelMonth.font = [UIFont systemFontOfSize:10];
        LabelMonth.transform = CGAffineTransformMakeRotation(M_PI * 0.3);
        [self addSubview:LabelMonth];
    }
    
}

#pragma mark 创建y轴数据
- (void)createLabelY{
    CGFloat Ydivision = 6;
    for (NSInteger i = 0; i < Ydivision; i++) {
        UILabel * labelYdivision = [[UILabel alloc]initWithFrame:CGRectMake(0, (self.frame.size.height - 2 * bounceY)/Ydivision *i + bounceX, bounceY, bounceY/2.0)];
        //   labelYdivision.backgroundColor = [UIColor greenColor];
        labelYdivision.tag = 2000 + i;
        labelYdivision.text = [NSString stringWithFormat:@"%.0f",(Ydivision - i)*100];
        labelYdivision.font = [UIFont systemFontOfSize:10];
        [self addSubview:labelYdivision];
    }
}

@end
