//
//  MyLayout.m
//  scanView
//
//  Created by Macmini on 2019/5/31.
//  Copyright © 2019 wenzbph. All rights reserved.
//

#import "MyLayout.h"

@implementation MyLayout
{
    NSMutableArray *_dataList;
}

- (void)prepareLayout{
    [super prepareLayout];
    _itemCount = (int)[self.collectionView numberOfItemsInSection:0];
    _dataList = [[NSMutableArray array] init];
    CGFloat radius = MIN(self.collectionView.frame.size.width, self.collectionView.frame.size.height);
    CGPoint center = CGPointMake(self.collectionView.frame.size.width/2, self.collectionView.frame.size.height/2);
    
    for (int i =0; i<_itemCount; i++) {
        UICollectionViewLayoutAttributes *atris = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:[NSIndexPath indexPathForItem:i inSection:0]];
        atris.size = CGSizeMake(50, 50);
        float x = center.x+cosf(2*M_PI/_itemCount*i)*(radius-25);
        float y = center.y+sinf(2*M_PI/_itemCount*i)*(radius-25);
        atris.center = CGPointMake(x, y);
        [_dataList addObject:atris];
    }
}

- (CGSize)collectionViewContentSize{
    return self.collectionView.frame.size;
}

- (NSArray<UICollectionViewLayoutAttributes *> *)layoutAttributesForElementsInRect:(CGRect)rect{
    return _dataList;
}

@end
