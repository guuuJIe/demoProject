//
//  jianbianView.m
//  scanView
//
//  Created by Macmini on 2019/8/6.
//  Copyright © 2019 wenzbph. All rights reserved.
//

#import "jianbianView.h"



@implementation jianbianView


- (instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
//        self.backgroundColor = [UIColor purpleColor];
        
        [self createText];
    }
    return self;
    
}

- (void)createText{
    

    
    
    self.titleLbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 64/2-6, 140, 35)];
    self.titleLbl.text = @"泊啤汇供应链";
    self.titleLbl.textColor = [UIColor whiteColor];
    self.titleLbl.font = [UIFont boldSystemFontOfSize:16];
    [self addSubview:self.titleLbl];

    UITextField *text1 = [UITextField new];
    text1.placeholder = @"大家都在搜:泰谷";
    text1.backgroundColor = [UIColor whiteColor];
    text1.frame = CGRectMake(15, self.frame.size.height - 30, self.frame.size.width - 15*2, 30);
    text1.layer.cornerRadius = 15;
    self.text = text1;
    [self addSubview:self.text];
    
    UIButton * messageBtn=[[UIButton alloc]initWithFrame:CGRectMake(Screen_Width - 41, 64/2-6, 34, 35)];
    [messageBtn setImage:[UIImage imageNamed:@"message2"] forState:(UIControlStateNormal)];
    [messageBtn addTarget:self action:@selector(scanAction) forControlEvents:UIControlEventTouchUpInside];
    [messageBtn setSelected:false];
    [self addSubview:messageBtn];
    
    UIButton * leftBtn=[[UIButton alloc]initWithFrame:CGRectMake(Screen_Width - 41 - 35, 64/2-6, 35, 35)];
    [leftBtn setImage:[UIImage imageNamed:@"scan"] forState:(UIControlStateNormal)];
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(4, 4, 4, 4);
    [leftBtn addTarget:self action:@selector(scanAction) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setSelected:false];
    [self addSubview:leftBtn];
    

}

- (void)scanAction{
    NSLog(@"点击了---");
}

////控制placeHolder的位置
//-(CGRect)placeholderRectForBounds:(CGRect)bounds
//{
//    CGRect inset = CGRectMake(bounds.origin.x+15, bounds.origin.y, bounds.size.width -15, bounds.size.height);
//    return inset;
//}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
