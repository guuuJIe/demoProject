//
//  CustomeView.m
//  scanView
//
//  Created by Macmini on 2019/7/20.
//  Copyright © 2019 wenzbph. All rights reserved.
//

#import "CustomeView.h"
#import "SecondView.h"
@interface CustomeView()

@property (nonatomic) SecondView *redView;

@end

@implementation CustomeView

- (instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
        
        [self initLayout];
        
    }
    
    return self;
    
}

- (void)initLayout{
    
    self.redView = [[SecondView alloc] initWithFrame:CGRectMake(0, Screen_Height, self.frame.size.width, 0)];
    self.redView.backgroundColor = [UIColor redColor];
    
    [self addSubview:self.redView];
   
}

- (void)initAnimation{
    
//    [self.redView.layer removeAllAnimations];
//
//    CATransition *animation = [CATransition animation];
//    animation.duration = 0.4f;
//    //    animation.timingFunction = [UIView AnimationCurveEaseInOut];
//    animation.fillMode = kCAFillModeForwards;
//    animation.type = kCATransitionPush;
//    animation.subtype = kCATransitionFromTop;
//    self.redView.frame = CGRectMake(0, Screen_Height - 140, self.frame.size.width, 140);
//    [self.redView.layer addAnimation:animation forKey:@"animation"];
    [UIView animateWithDuration:0.5 animations:^{
        //将view.frame 设置在屏幕上方
        self.redView.frame = CGRectMake(0, Screen_Height - 140, self.frame.size.width, 140);
    }];
}

- (void)exitAni{
//    [self.redView.layer removeAllAnimations];
//
//    CATransition *animation = [CATransition animation];
//    animation.duration = 0.4f;
//    //    animation.timingFunction = [UIView AnimationCurveEaseInOut];
//    animation.fillMode = kCAFillModeBackwards;
//    animation.type = kCATransitionMoveIn;
//    animation.subtype = kCATransitionFromBottom;
//    self.redView.frame = CGRectMake(0, Screen_Height, self.frame.size.width, 140);
//
//    [self.redView.layer addAnimation:animation forKey:@"animation"];
    [UIView animateWithDuration:0.5 animations:^{
       self.redView.frame = CGRectMake(0, Screen_Height, self.frame.size.width, 140);
    } completion:^(BOOL finished) {
        if (finished) {
            [self removeFromSuperview];
        }
    }];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
//    [UIView animateWithDuration:0.5 animations:^{
//
//    } completion:^(BOOL finished) {
//        [self removeFromSuperview];
//    }];
//    [super touchesBegan:touches withEvent:event];
     [self exitAni];
}

- (void)show{
    
    [_contrc.view addSubview:self];
    [self initAnimation];
    
}


- (void)dimiss{
    
//    [UIView animateWithDuration:0.5 animations:^{
//        [self exitAni];
//    } completion:^(BOOL finished) {
//        if (finished) {
//            [self removeFromSuperview];
//        }
//
//    }];
    
}

@end
