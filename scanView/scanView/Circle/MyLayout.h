//
//  MyLayout.h
//  scanView
//
//  Created by Macmini on 2019/5/31.
//  Copyright © 2019 wenzbph. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyLayout : UICollectionViewLayout

@property(nonatomic,assign)int itemCount;

@end

NS_ASSUME_NONNULL_END
