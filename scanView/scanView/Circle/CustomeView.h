//
//  CustomeView.h
//  scanView
//
//  Created by Macmini on 2019/7/20.
//  Copyright © 2019 wenzbph. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CustomeView : UIView

- (void)show;

@property (nonatomic) UIViewController *contrc;

- (void)dimiss;
@end

NS_ASSUME_NONNULL_END
