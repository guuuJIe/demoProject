//
//  SocketRocketUtility.h
//  scanView
//
//  Created by Macmini on 2019/1/26.
//  Copyright © 2019 wenzbph. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SocketRocketUtility : NSObject
//单利
+ (SocketRocketUtility *)instace;
//开启连接
- (void)SRWebSocketOpenWithURLString:(NSString *)urlString;
@end
