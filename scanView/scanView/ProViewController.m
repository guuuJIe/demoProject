//
//  ProViewController.m
//  scanView
//
//  Created by Macmini on 2019/3/2.
//  Copyright © 2019 wenzbph. All rights reserved.
//

typedef void(^block)(void);

#import "ProViewController.h"
#import "MyLayout.h"
#import "NSObject+model.h"
@interface ProViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic) block myblock;

@end

@implementation ProViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSLog(@"%@",self.data);
    
    __block int val = 10;
    void (^blk)(void) = ^{
        [self desgin];
        printf("val = %d\n,",val);
    };
    val = 2;
    blk();
    
//     __weak id weakSelf = self;
    __weak typeof (self)weslf = self;
    
    self.myblock = ^{
        [weslf desgin];
    };
    
    
//    1.并行队列===可以让多个任务并发（同时）执行。（可以开启多个线程，并且同时执行任务）======并发队列的并发功能只有在异步（dispatch_async）函数下才有效
//    2.串行队列===每次只有一个任务被执行。让任务一个接着一个地执行。（只开启一个线程，一个任务执行完毕后，再执行下一个任务）
//    dispatch_queue_t q = dispatch_queue_create("cc_queue2", DISPATCH_QUEUE_CONCURRENT);
//    dispatch_sync(q,^{
//        NSLog(@"用户登录 %@",[NSThread currentThread]);
//    });
//
//    //2.支付
//    dispatch_async(q,^{
//        NSLog(@"支付 %@",[NSThread currentThread]);
//    });
//
//    //3.下载
//    dispatch_async(q,^{
//        NSLog(@"下载  %@",[NSThread currentThread]);
//    });
    
    dispatch_queue_t serialQueue = dispatch_queue_create("com.lai.www", DISPATCH_QUEUE_SERIAL);

//    dispatch_async(serialQueue, ^{
//        // NSLog(@"1");
//        sleep(3);
//        NSLog(@"1%@",[NSThread currentThread]);
//    });
    dispatch_sync(serialQueue, ^{

//        sleep(1);
        NSLog(@"2%@",[NSThread currentThread]);

    });
//    dispatch_async(serialQueue, ^{
//        NSLog(@"3%@",[NSThread currentThread]);
//    });
    dispatch_sync(serialQueue, ^{
//        sleep(5);
        NSLog(@"4%@",[NSThread currentThread]);
    });
//
//    dispatch_async(serialQueue, ^{
//
//        NSLog(@"5%@",[NSThread currentThread]);
//    });
    
    

    NSTimer* timer = [NSTimer scheduledTimerWithTimeInterval: 1
                                                      target: self
                                                    selector: @selector(handleTimer:)
                                                    userInfo: @{@"name":@"testTimer"}
                                                     repeats: YES];
   
    
   
//    [self initLayout];
    
    
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        for (int i = 0; i<2; i++) {
//            NSLog(@"%d",i);
//        }
//        
//        NSLog(@"%@",[NSThread currentThread]);
//    });
    
}

- (void)handleTimer:(NSTimer*)timer{
//    id userInfo = timer.userInfo;
    NSLog(@"handleTimer");
}

- (void)desgin{
    
}


- (void)dealloc{
    
    
}


- (void)initLayout{
    MyLayout *layout = [[MyLayout alloc] init];
    UICollectionView *collect = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, 320, 400) collectionViewLayout:layout];
    collect.delegate = self;
    collect.dataSource = self;
    collect.backgroundColor = [UIColor whiteColor];
    [collect registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellid"];

    [self.view addSubview:collect];
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{

    return 1;

}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 10;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellid" forIndexPath:indexPath];
    cell.layer.masksToBounds = true;
    cell.layer.cornerRadius = 25;
    cell.backgroundColor = [UIColor colorWithRed:arc4random()%255/225.0 green:arc4random()%255/225.0 blue:arc4random()%255/225.0 alpha:1];
    return cell;

}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
