//
//  Person.h
//  scanView
//
//  Created by Macmini on 2019/1/8.
//  Copyright © 2019 wenzbph. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject

@property (nonatomic, strong)NSString *name;
@property (nonatomic, copy) void (^getCardInfo)(NSDictionary *cardInfo);
- (void)eat:(NSString *)num;

+ (void)buy:(NSString *)goods;

- (void)buy;
@end
