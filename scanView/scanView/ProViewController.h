//
//  ProViewController.h
//  scanView
//
//  Created by Macmini on 2019/3/2.
//  Copyright © 2019 wenzbph. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^myBlock)(void);

NS_ASSUME_NONNULL_BEGIN

@interface ProViewController : UIViewController

@property (nonatomic,copy)myBlock block;
@property (nonatomic) id data;
@end

NS_ASSUME_NONNULL_END
