//
//  goodsModel.h
//  scanView
//
//  Created by Macmini on 2018/12/19.
//  Copyright © 2018年 wenzbph. All rights reserved.
//

#import <Foundation/Foundation.h>
@class goodsItem;
@class goodsInfo;

@interface goodsModel : NSObject
@property (nonatomic,copy)NSString *order_sn;
@property (nonatomic,copy)NSString *age;
@property (nonatomic,strong)NSArray *goodsItem;
@end


@interface goodsItem : NSObject
@property(nonatomic,copy) NSString *name;
@property(nonatomic,copy) NSString *sub_name;
@property(nonatomic,strong) NSDictionary *goodsInfo;
@end

@interface goodsInfo : NSObject
@property(nonatomic,copy) NSString *name;
@property(nonatomic,copy) NSString *sub_name;
@end
