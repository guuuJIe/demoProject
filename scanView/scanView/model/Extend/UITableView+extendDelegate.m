//
//  UITableView+extendDelegate.m
//  scanView
//
//  Created by Macmini on 2019/7/31.
//  Copyright © 2019 wenzbph. All rights reserved.
//

#import "UITableView+extendDelegate.h"
#import "NSObject+GetProperty.h"
#import <objc/runtime.h>

static NSString *nameKey = @"namekey";


@implementation UITableView (extendDelegate)


//- (id)initWithServerData:(NSArray *)serverData
//      andCellIdentifiers:(NSArray *)identifiers
//{
//    self = [super init];
//    if(self)
//    {
//        self.serverData = serverData;           // 数据
//        self.cellIdentifiers = identifiers;     // 复用
//    }
//
//    return self;
//}

- (void)setDelegateWithUITableView:(UIScrollView *)scrollview withUINaviagtinBar:(UINavigationBar *)bar withjianbianView:(jianbianView *)view{
    
    
   
    scrollview.delegate = self;
    
    [bar setBackgroundImage:[UIImage imageNamed:@"Image"] forBarMetrics:UIBarMetricsDefault];
    
}

-(void)setJbView:(jianbianView *)jbView{
    
    objc_setAssociatedObject(self, &nameKey, jbView, OBJC_ASSOCIATION_COPY);
    
}

- (jianbianView *)jbView{
    
    return objc_getAssociatedObject(self, &nameKey);
}



- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    jianbianView *jbview = self.jbView;
    CGFloat offsetY = scrollView.contentOffset.y;
    CGFloat radio;
    
    if (offsetY > 0 && offsetY<64) {
        radio = (offsetY+20)/150;
    }
   
//    jbview.frame = CGRectMake(0, offsetY, Screen_Width - 120*radio, 100);
    NSLog(@"%f",scrollView.contentOffset.y);
    
}



//- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    return 6;
//}

@end
