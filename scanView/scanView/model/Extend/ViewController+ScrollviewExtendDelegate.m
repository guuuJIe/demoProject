//
//  ViewController+ScrollviewExtendDelegate.m
//  scanView
//
//  Created by Macmini on 2019/8/15.
//  Copyright © 2019 wenzbph. All rights reserved.
//

#import "ViewController+ScrollviewExtendDelegate.h"

#define marginleft 15

@implementation ViewController (ScrollviewExtendDelegate)



- (void)scrollViewDidScrollToTop:(UIScrollView *)scrollView{
    NSLog(@"滚到顶部");
    CGFloat alpha = 1;
    

    [UIView animateWithDuration:0.2 animations:^{
        
        self.topView.frame = CGRectMake(0, 0, Screen_Width, 120);
        self.topView.text.frame = CGRectMake(15, self.topTextMinY, Screen_Width-30, 30);
        self.customeView.alpha = alpha - 1;
        self.topView.alpha = alpha;
        self.topView.titleLbl.alpha = alpha;
    }];
}

-(BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView{
    
    return true;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    CGFloat offSetY = scrollView.contentOffset.y;
//    NSLog(@"ViewDidScroll   %f      %f  %f",offSetY,self.linjiePoint,self.offSetY);
    if (offSetY > 0 && offSetY <= self.linjiePoint) {
        if (self.offSetY == self.linjiePoint) {
            offSetY = self.offSetY;
        }
        
      __block   CGFloat index = offSetY/self.space;

        
      [UIView animateWithDuration:0.5 animations:^{
          
            self.topView.text.frame = CGRectMake(marginleft, self.topTextMinY-offSetY-6, (Screen_Width - marginleft*2-self.bwidth*(offSetY/self.space)), 30);

            
            if (offSetY == self.linjiePoint) {
                index = 1;
            }
            self.customeView.alpha = index;
            self.topView.titleLbl.alpha = 1;

        }];
        
        
        
    }else if (offSetY<=0){
        

        CGFloat alpha;
        if (offSetY == 0.00) {
            alpha = 1;
        }else{
            alpha = 0;
        }
        
        [UIView animateWithDuration:0.2 animations:^{
            
            self.topView.frame = CGRectMake(0, 0, Screen_Width, 120);
            self.topView.text.frame = CGRectMake(marginleft, self.topTextMinY, Screen_Width-30, 30);
            self.customeView.alpha = 0;
            self.topView.alpha = alpha;
            self.topView.titleLbl.alpha = alpha;
        }];
        
        
    }else{
//        offSetY = self.linjiePoint;
//
//        [UIView animateWithDuration:0.5 animations:^{
//            self.customeView.alpha = 0;
//            self.topView.text.frame = CGRectMake(marginleft, self.navTextMinY, (Screen_Width - marginleft*2-self.bwidth*(offSetY/self.space)), 30);
//            self.topView.alpha = 1;
//            self.topView.text.alpha = 1;
//
//        }];
    }
    
    
}

//已经减速的时候

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{

    CGFloat offSetY = scrollView.contentOffset.y;
    if (offSetY > (self.linjiePoint/2)){
        offSetY = self.linjiePoint;
//        CGFloat radio = offSetY/self.space;
//        NSLog(@"已经暂停--%f  %f",self.navTextMinY,offSetY);
        [UIView animateWithDuration:0.5 animations:^{
            self.customeView.alpha = 1;
//            self.topView.frame = CGRectMake(0, 0, Screen_Width, 150 - (64*radio));
            self.topView.text.frame = CGRectMake(marginleft, self.navTextMinY, (Screen_Width - marginleft*2-self.bwidth*(offSetY/self.space)), 30);
//            self.topView.alpha = 0;
//            self.topView.alpha = 1;
        }];

    }else{


//        self.topView.alpha = 1;
//        self.topView.text.alpha = 1;
        self.topView.frame = CGRectMake(0, 0, Screen_Width, 120);
        self.topView.text.frame = CGRectMake(marginleft, self.topTextMinY, Screen_Width-(marginleft*2), 30);
//        self.customeView.alpha = 0;
//        self.customeView.text.alpha = 0;


    }

}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    CGFloat offSetY = scrollView.contentOffset.y;

    if (offSetY <0) {
//        CGFloat radio = (offSetY+20)/140;
//        NSLog(@"未形成导航 %f",radio);
        [self.scrollTable scrollRectToVisible:CGRectMake(0, 0, Screen_Width, Screen_Height) animated:YES];
//        self.topView.text.alpha = 1-(radio*1.8);

    }else{
       
        offSetY = self.linjiePoint;
        self.offSetY = offSetY;
//        CGFloat radio = offSetY/self.space;
//        NSLog(@"形成导航--%f  %f",self.navTextMinY,offSetY);
        [UIView animateWithDuration:0.5 animations:^{
            self.customeView.alpha = 1;
            self.topView.text.frame = CGRectMake(marginleft, self.navTextMinY, (Screen_Width - marginleft*2-self.bwidth*(offSetY/self.space)), 30);
            self.topView.alpha = 1;
            self.topView.text.alpha = 1;

        }];
    }
}

@end
