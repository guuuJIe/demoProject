//
//  NSObject+model.m
//  scanView
//
//  Created by Macmini on 2019/1/7.
//  Copyright © 2019 wenzbph. All rights reserved.
//

#import "NSObject+model.h"
#import <objc/message.h>
@implementation NSObject (model)
+(instancetype)initModelWithDict:(NSDictionary *)dic{
    id objc = [[self alloc] init];
    
    unsigned int count = 0;
    Ivar *ivarList = class_copyIvarList([objc class], &count);
    
    for (int i = 0; i<count; i++) {
        Ivar ivar = ivarList[i];
        const char *name = ivar_getName(ivar);
        
        NSString *key = [[[NSString alloc]initWithUTF8String:name] substringFromIndex:1];
        NSLog(@"%@",key);
        
        NSString *ivarType = [NSString stringWithUTF8String:ivar_getTypeEncoding(ivar)];
        NSLog(@"%@",ivarType);
        
        id value = dic[key];
        
        if ([value isKindOfClass:[NSArray class]] && value) {
            NSLog(@"%@,%@",key,dic[key]);
            NSMutableArray *arrDataSource = [NSMutableArray arrayWithCapacity:0];
            Class mySub = NSClassFromString(key);
            for (NSDictionary *subDic in dic[key]) {
                id subValue = [mySub initModelWithDict:subDic];
                [arrDataSource addObject:subValue];
            }
            value = arrDataSource;
        }
        
        if ([value isKindOfClass:[NSDictionary class]] && value) {
//            NSLog(@"%@",key);
            Class mySub = NSClassFromString(key);
            value = [mySub initModelWithDict:value];
        }
        
        
        if (value) {
            [objc setValue:value forKey:key];
        }
        
    }
    
    return objc;
    
}

//- (BOOL)willDealloc {
//    __weak id weakSelf = self;
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        [weakSelf assertNotDealloc];
//    });
//    return YES;
//}
//- (void)assertNotDealloc {
//    NSAssert(NO, @"");
//}
@end
