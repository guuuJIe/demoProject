//
//  Person+UsePrivateMethod.h
//  scanView
//
//  Created by Macmini on 2019/7/17.
//  Copyright © 2019 wenzbph. All rights reserved.
//

#import "Person.h"

NS_ASSUME_NONNULL_BEGIN

@interface Person (UsePrivateMethod)
- (void)eat:(NSInteger)num;
@end

NS_ASSUME_NONNULL_END
