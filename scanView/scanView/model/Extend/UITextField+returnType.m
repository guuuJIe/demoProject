//
//  UITextField+returnType.m
//  scanView
//
//  Created by Macmini on 2019/1/21.
//  Copyright © 2019 wenzbph. All rights reserved.
//

#import "UITextField+returnType.h"
#import <objc/message.h>

@implementation UITextField (returnType)

//- (instancetype)initWithFrame:(CGRect)frame{
//    if (self = [super initWithFrame:frame]) {
////       self.keyboardType
//        NSLog(@"初始化了");
//        self.returnKeyType = UIReturnKeyDone;
//    }
//    return self;
//}
//
//- (void)setReturnKeyType:(UIReturnKeyType)returnKeyType{
//    returnKeyType = UIReturnKeyDone;
//}

//- (NSInteger)returnKeyType{
//    if (self.KeyType) {
//        return self.returnKeyType = self.KeyType;
//    }
//
//    return self.returnKeyType = UIReturnKeyDone;
//}

//- (NSInteger)key{
//    if (self.key) {
//        return self.key;
//    }else{
//        return UIReturnKeySend;
//    }
//}
//
//- (void)setKey:(NSInteger)key{
//    objc_setAssociatedObject(self, @selector(key), @(key), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
//}

//- (void)setKeyType:(NSInteger)KeyType{
//    objc_setAssociatedObject(self, @selector(KeyType), @(KeyType), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
//}
//
//
//- (BOOL)isReturnKeyType{
//    NSNumber * num = objc_getAssociatedObject(self, _cmd);
//    return (num && [num isKindOfClass:NSNumber.self]) ? [num boolValue] : NO;
//}]
//- (instancetype)initWithFrame:(CGRect)frame{
//    method_exchangeImplementations(class_getInstanceMethod(self.class, NSSelectorFromString(@"setReturnKeyType:")), class_getInstanceMethod(self.class, @selector(swizzledSetReturnKeyType:)));
//    if (self = [super initWithFrame:frame]) {
//       
//    }
//    return self;
//    
//}

+(void)load{
    NSLog(@"switch");
    Method initframe = class_getInstanceMethod(self.class, @selector(initWithFrame:));
    Method swizzled = class_getInstanceMethod(self.class, @selector(swizzledinitWithFrame:));
    //runtime进行交换即可  完成交换动作
    
    method_exchangeImplementations(initframe, swizzled);
    
    
    Method initCode = class_getInstanceMethod(self.class, @selector(initWithCoder:));
    Method swizzleinitCode = class_getInstanceMethod(self.class, @selector(swizzledinitWithCoder:));
    method_exchangeImplementations(initCode, swizzleinitCode);
}



- (instancetype)swizzledinitWithCoder:(NSCoder *)coder{
    [self swizzledinitWithCoder:coder];
    NSLog(@"===================swizzledinitWithCoder");
    return self;
}


- (instancetype)swizzledinitWithFrame:(CGRect)frame{
    [self swizzledinitWithFrame:frame];
    self.returnKeyType = UIReturnKeyDone;
    self.delegate = self;
    return self;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self resignFirstResponder];
    return true;
}

@end
