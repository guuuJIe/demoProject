//
//  NSObject+model.h
//  scanView
//
//  Created by Macmini on 2019/1/7.
//  Copyright © 2019 wenzbph. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (model)//NSObject

+(instancetype)initModelWithDict:(NSDictionary *)dic;

- (BOOL)willDealloc;
@end
