//
//  NSObject+GetProperty.h
//  scanView
//
//  Created by Macmini on 2019/3/2.
//  Copyright © 2019 wenzbph. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSObject (GetProperty)
+ (NSArray *)jl_getProperties;
@end

NS_ASSUME_NONNULL_END
