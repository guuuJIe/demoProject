//
//  UIImage+Common.h
//  scanView
//
//  Created by Macmini on 2019/8/6.
//  Copyright © 2019 wenzbph. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImage (Common)
+(UIImage *)imageWithColor:(UIColor *)aColor;
@end

NS_ASSUME_NONNULL_END
