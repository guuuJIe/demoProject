//
//  UIScrollView+gradient.h
//  scanView
//
//  Created by Macmini on 2019/7/31.
//  Copyright © 2019 wenzbph. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIScrollView (gradient)<UIScrollViewDelegate>


- (void)initWithScrollView:(UIScrollView *)scrollview;

@end

NS_ASSUME_NONNULL_END
