//
//  NSDictionary+NilSafe.h
//  scanView
//
//  Created by Macmini on 2019/7/26.
//  Copyright © 2019 wenzbph. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSDictionary (NilSafe)

@end

NS_ASSUME_NONNULL_END
