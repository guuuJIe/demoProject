//
//  NSObject+GetProperty.m
//  scanView
//
//  Created by Macmini on 2019/3/2.
//  Copyright © 2019 wenzbph. All rights reserved.
//

#import "NSObject+GetProperty.h"
#import <objc/message.h>
@implementation NSObject (GetProperty)

+ (NSArray *)jl_getProperties {
    //用于存入属性数量
    unsigned int outCount = 0;
    //获取属性数组
    objc_property_t *propertyList = class_copyPropertyList([self class], &outCount);
    
    NSMutableArray *arrM = [NSMutableArray arrayWithCapacity:outCount];
    //遍历数组
    for (int i = 0; i < outCount; ++i) {
        objc_property_t property = propertyList[i];
        //获取属性名
        const char *cName = property_getName(property);
        //将其转换成c字符串
        NSString *propertyName = [NSString stringWithCString:cName encoding:NSUTF8StringEncoding];
        //        加入数组
        [arrM addObject:propertyName];
    }
    //在使用了c函数的creat, copy等函数是记得手动释放,要不然会引起内存泄露问题
    free(propertyList);
    return arrM.copy;
    
}

@end
