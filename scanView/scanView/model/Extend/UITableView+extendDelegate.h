//
//  UITableView+extendDelegate.h
//  scanView
//
//  Created by Macmini on 2019/7/31.
//  Copyright © 2019 wenzbph. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class jianbianView;

@interface UITableView (extendDelegate)<UIScrollViewDelegate>

- (void)setDelegateWithUITableView:(UIScrollView *)scrollview withUINaviagtinBar:(UINavigationBar *)bar withjianbianView:(jianbianView *)view;
@property (nonatomic)jianbianView *jbView;
@end

NS_ASSUME_NONNULL_END
