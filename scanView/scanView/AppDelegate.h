//
//  AppDelegate.h
//  scanView
//
//  Created by Macmini on 2018/12/13.
//  Copyright © 2018年 wenzbph. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

